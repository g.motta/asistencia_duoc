

let sql = require("msnodesqlv8");
let jwt = require('jsonwebtoken');
let config = require('./../../config.json').sqlServer;
let userModel = require('../model/usuario');


const login = (usuarioParam, password) => {

    return new Promise((resolve, reject) => {
        let query = userModel.query(usuarioParam, password);
        sql.query(config.conexion, query, (err, usuario) => {

            if (err) reject(err);
            let jsonresponse = {};
       
            if (usuario) {

                let token = jwt.sign({
                    data: usuario
                }, 'secret', { expiresIn: 60 * 60 });

                jsonresponse = {
                    usuario: usuario[0],
                    token
                }
            } else {
                jsonresponse = {
                    usuario: `no existe el usuario ${usuarioParam} en la base de datos o su contraseña `,
                    token: false
                }
            }

            resolve(jsonresponse);






        });


    });


}



module.exports = {
    login
};

