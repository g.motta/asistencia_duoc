
let express = require('express');
let bodyParser = require('body-parser')
let sql = require("msnodesqlv8");
let app = express();
let usuario = require('./controller/usuario');
let config = require('./../config').sqlServer;

//variables para conectar.
const connectionString = config.conexion;




// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

/**Trae los alumnos por materia */
app.get('/obtenerAlumnosPorMateria', async function (req, res) {


    let query = `  SELECT 
    MA_ALUMNO.NOMBRE,
    MA_ALUMNO.APELLIDO,
    MA_ALUMNO.RUT,
    MA_MATERIA.DESCRIPCION AS nombre_materia,
    MA_MATERIA.SECCION,
    RE_ALUMNO_MATERIA.ID_ALUMNO,
    RE_ALUMNO_MATERIA.ID_MATERIA
    FROM RE_ALUMNO_MATERIA
    INNER JOIN MA_ALUMNO ON MA_ALUMNO.RUT = RE_ALUMNO_MATERIA.ID_ALUMNO
    INNER JOIN MA_MATERIA ON MA_MATERIA.ID_MATERIA = RE_ALUMNO_MATERIA.ID_MATERIA
    WHERE RE_ALUMNO_MATERIA.ID_MATERIA =  ${req.query.id_materia};`;

    await sql.query(connectionString, query, (err, rows) => {

        return res.json(rows)
    });




});
/**
 * Login se le debe pasar el rut y apellido.
 */

app.post('/login', function (req, res) {

    usuario.login(req.query.username, req.query.password).then(response => {
        console.log(response);
        res.json({
            respuesta: response,
            status: true
        });
    }).catch(error => {

        res.json({
            respuesta: error,
            status: false
        });
    });



});

/** Se obtiene la materia en base al id del profesor creado en sistema. */
app.get('/obtenerMaterias', async function (req, res) {


    let queryMateria = `SELECT 
    MA_MATERIA.NOMBRE AS NOMBRE_MATERIA,
    MA_MATERIA.SECCION,
	MA_MATERIA.ID_MATERIA
    FROM 
    RE_MATERIA_PROFESOR RMP
    INNER JOIN MA_PROFESOR ON MA_PROFESOR.ID_PROFESOR = RMP.ID_PROFESOR
    INNER JOIN MA_MATERIA ON MA_MATERIA.ID_MATERIA = RMP.ID_MATERIA
    WHERE RMP.ID_PROFESOR  = ${req.query.id_materia}`

    await sql.query(connectionString, queryMateria, (err, materias) => {

        return res.json(materias)
    });




});

/** Sirve para registrar la asistencía del alumno */
app.post('/registrarAsistencia', async function (req, res) {

    console.log(req.query);

    const query = `EXECUTE registraAsistencia   @ID_ALUMNO = ${req.query.ID_ALUMNO}, 
    @ID_MATERIA = ${req.query.ID_MATERIA}, 
    @PRESENTE = ${req.query.PRESENTE};  `

    //const query = `INSERT INTO MA_ASISTENCIA(ID_ASISTENCIA,ID_ALUMNO, ID_MATERIA,PRESENTE)
    //VALUES (NEXT VALUE FOR SEC_ASISTENCIA,, ,);`

    await sql.query(connectionString, query, (err, rows) => {

        if (err) {
            res.json({
                valor: false,
                message: req.query
            })
            console.log(err);
        } else {
            return res.json({
                valor: true,
                message: "usuario ingresado con exito."
            });
        }

    });
});

/** sirve para obtener la cantidad de asistencía por alumno y materia. */
app.get('/obtenerAsistencia', async function (req, res) {
    query = `SELECT A.ID_ALUMNO, B.PRESENTE, C.AUSENTE, B.nombre_materia, B.SECCION , A.nombre + ' ' + A.apellido NOMBRE, A.RUT, A.APELLIDO FROM MA_ALUMNO A,
    (
    SELECT ID_ALUMNO, COUNT(PRESENTE) PRESENTE, mam.NOMBRE as nombre_materia, mam.SECCION FROM MA_ASISTENCIA as  mas
	inner join MA_MATERIA mam on mam.ID_MATERIA  = mas.ID_MATERIA
	WHERE PRESENTE = 1
	AND mas.ID_MATERIA = ${req.query.id_materia}
    GROUP BY ID_ALUMNO, PRESENTE, mam.NOMBRE, mam.SECCION
    ) B,
    (
   SELECT ID_ALUMNO, COUNT(PRESENTE) AUSENTE, mam.NOMBRE as nombre_materia, mam.SECCION FROM MA_ASISTENCIA as  mas
	inner join MA_MATERIA mam on mam.ID_MATERIA  = mas.ID_MATERIA
	WHERE PRESENTE = 0
	AND mas.ID_MATERIA = ${req.query.id_materia}
    GROUP BY ID_ALUMNO, PRESENTE, mam.NOMBRE, mam.SECCION
    ) C
    WHERE A.ID_ALUMNO = B.ID_ALUMNO
      AND A.ID_ALUMNO = C.ID_ALUMNO
    GROUP BY A.ID_ALUMNO, B.PRESENTE, C.AUSENTE, A.nombre, A.apellido, A.rut, B.nombre_materia,B.SECCION
`

    await sql.query(connectionString, query, (err, rows) => {


        return res.json(rows)
    });


});

app.listen(3000, function () {
    console.log("funcionando en el puerto 3000..");
});




